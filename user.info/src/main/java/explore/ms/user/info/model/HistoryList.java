package explore.ms.user.info.model;

import java.util.List;

public class HistoryList {

	private List<History> histories;
	
	public HistoryList() {
		super();
	}

	public HistoryList(List<History> histories) {
		super();
		this.histories = histories;
	}

	public List<History> getHistories() {
		return histories;
	}

	public void setHistories(List<History> histories) {
		this.histories = histories;
	}
	
}
