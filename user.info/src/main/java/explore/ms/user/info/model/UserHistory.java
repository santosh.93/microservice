package explore.ms.user.info.model;

import java.util.List;

public class UserHistory {

	private User user;
	private List<History> history;	
	
	public UserHistory() {
		super();
	}

	public UserHistory(User user, List<History> history) {
		super();
		this.user = user;
		this.history = history;
	}

	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public List<History> getHistory() {
		return history;
	}
	
	public void setHistory(List<History> history) {
		this.history = history;
	}	
	
}
