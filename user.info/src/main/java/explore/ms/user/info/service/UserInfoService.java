package explore.ms.user.info.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import explore.ms.user.info.model.User;
import explore.ms.user.info.model.UserHistory;

@Service
public class UserInfoService {

	private static String USER_SERVICE_URL = "http://localhost:8082/user";
	private static String USER_HISTORY_URL = "http://localhost:8080/history";
	
	@Autowired
	private RestTemplate restTemplate;
	
	public UserHistory getUserInfo(String userId) {
		
		ResponseEntity<User> user = restTemplate.getForEntity(USER_SERVICE_URL 
											+ "/" + userId, User.class);
		
		@SuppressWarnings("rawtypes")
		ResponseEntity<List> historyList = restTemplate.getForEntity(USER_HISTORY_URL 
											+ "/" + userId, List.class);
		
		@SuppressWarnings("unchecked")
		UserHistory userHistory = new UserHistory(user.getBody(), 
				historyList.getBody());
		return userHistory;
	}
	
}
