package explore.ms.user.info;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import explore.ms.user.info.service.UserInfoService;

@SpringBootApplication
public class Application {

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public UserInfoService getUserInfoService() {
		return new UserInfoService();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
