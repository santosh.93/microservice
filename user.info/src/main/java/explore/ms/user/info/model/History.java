package explore.ms.user.info.model;

public class History {
	
	private String contentId;
	private String description;
	
	public History() {
		super();
	}

	public History(String contentId, String description) {
		super();
		this.contentId = contentId;
		this.description = description;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
