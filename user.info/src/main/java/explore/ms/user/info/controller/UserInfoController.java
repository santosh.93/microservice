package explore.ms.user.info.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import explore.ms.user.info.model.UserHistory;
import explore.ms.user.info.service.UserInfoService;

@RestController
@RequestMapping("/userinfo")
public class UserInfoController {

	@Autowired
	private UserInfoService userInfoService;
	
	@RequestMapping(value = "/{userId}" , method = RequestMethod.GET)
	public UserHistory getUserInfo(@PathVariable("userId") String userId) {
		return userInfoService.getUserInfo(userId);
	}
	
}
