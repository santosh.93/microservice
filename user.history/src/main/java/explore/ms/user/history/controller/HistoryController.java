package explore.ms.user.history.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import explore.ms.user.history.model.History;
import explore.ms.user.history.service.HistoryService;

@RestController
@RequestMapping("/history")
public class HistoryController {

	@Autowired
	private HistoryService historyService;
	
	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public List<History> getHistory(@PathVariable("userId") String userId) {
		return historyService.getHistory(userId);
	}
	
}
