package explore.ms.user.history.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import explore.ms.user.history.model.History;

@Service
public class HistoryService {

	private Map<String, List<History>> userHistories = new HashMap<String, List<History>>();
	
	public HistoryService() {
		History history1 = new History("google.ai", "Google AI");
		History history2 = new History("facebook", "Facebook");
		
		List<History> histories = new ArrayList<History>();
		histories.add(history1);
		histories.add(history2);
		
		List<History> histories2 = new ArrayList<History>();
		histories2.add(history1);
		
		userHistories.put("1", histories);
		userHistories.put("2", histories2);
		
	}
	
	public List<History> getHistory(String userId) {
		Optional<String> optional = userHistories.keySet()
												  .stream()
												  .filter(key -> key.equals(userId))
												  .findFirst();
		return optional.isPresent() ? userHistories.get(optional.get()) : new ArrayList<History>();
													
	}
	
}
