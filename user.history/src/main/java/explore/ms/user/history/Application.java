package explore.ms.user.history;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import explore.ms.user.history.service.HistoryService;

@SpringBootApplication
public class Application {

	@Bean
	public HistoryService getHistoryService() {
		return new HistoryService();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
