package explore.ms.user.details;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import explore.ms.user.details.service.UserService;

@SpringBootApplication
public class Application {

	@Bean
	public UserService getUserService() {
		return new UserService();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
