package explore.ms.user.details.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import explore.ms.user.details.model.User;

@Service
public class UserService {

	private List<User> users = Arrays.asList(new User("1", "First", "Last"),
			new User("2", "Rahul", "D"));
	
	public User getUser(String userId) {
		Optional<User> user = users.stream()
								   .filter(u -> u.getUserid().equalsIgnoreCase(userId))
								   .findFirst();
		return user.isPresent() ? user.get() : new User("0", "Dummy", "Dummy");
	}
	
}
