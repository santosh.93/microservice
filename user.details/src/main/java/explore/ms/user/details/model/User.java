package explore.ms.user.details.model;

public class User {
	
	private String userid;
	private String firstName;
	private String lastName;
	
	public User() {
		super();
	}

	public User(String userid, String firstName, String lastName) {
		super();
		this.userid = userid;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getUserid() {
		return userid;
	}
	
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
